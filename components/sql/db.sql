CREATE TABLE `jos_emails_emails` (
  `email_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_sent` datetime NOT NULL,
  `from` varchar(255) NOT NULL DEFAULT '',
  `to` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) DEFAULT NULL,
  `html` text NOT NULL,
  `text` text NOT NULL,
  `sent` TINYINT(1) NOT NULL DEFAULT '0',
  `message` varchar(255) DEFAULT '',
  `read` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `jos_emails_layouts` (
  `layout_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `html` text,
  `text` text,
  PRIMARY KEY (`layout_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `jos_emails_templates` (
  `template_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `component` varchar(20) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `from_name` varchar(50) DEFAULT NULL,
  `from_email` varchar(255) DEFAULT NULL,
  `body_html` text,
  `body_text` text,
  `layout_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `identifier` (`component`,`type`),
  KEY `layout_id` (`layout_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


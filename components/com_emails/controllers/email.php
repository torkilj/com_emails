<?php
/**
 * Com Emails Emails Controller
 *
 * @author      Kyle Waters <kyle@organic-development.com>
 * @author      Mark Head <mark@organic-development.com>
 * @category    Controller
 * @package     Emails
 * @uses        Com_Organic
 */
class ComEmailsControllerEmail extends ComEmailsControllerDefault
{
    /**
     * Initalize the class
     *
     * @param KConfig $config
     */
    protected function _initialize( KConfig $config )
    {
        parent::_initialize( $config );
        $config->behaviors = array_diff( KConfig::unbox( $config->behaviors ), array( 'executable' ) );
    }

    /**
     * Specialised display function.
     *
     * @param	KCommandContext	A command context object
     * @return 	string|false 	The rendered output of the view or false if something went wrong
     */
    protected function _actionGet(KCommandContext $context)
    {
        $context->setError(new KControllerException(JText::_( 'EMAILS_METHOD_NOT_ALLOWED' ), KHttpResponse::METHOD_NOT_ALLOWED));
    }

    /**
     * Send email
     *
     * @param KCommandContext $context
     * @return bool
     */
    public function _actionSend(KCommandContext $context)
    {
        // Get an instance of the template model
        $template_controller = $this->getService('com://site/emails.controller.template');
        $template = $template_controller->getModel()->set(array('id' => $context->data->template_id, 'component' => $context->data->component, 'type' => $context->data->type))->getItem();

        $html = (isset($context->data->html)) ? $context->data->html : true;

        // Set the mailer
        $mailer =& JFactory::getMailer();
        $mailer->isHTML($html);

        // Set up email sender
        if($template->from_name && $template->from_email)
        {
            $mailer->setSender(
                array(
                    $template->from_email,
                    $template->from_name
                )
            );
        }

        else if($template->from_email)
        {
            $mailer->setSender($template->from_email);
        }

        // Set the email subject
        $subject = $template->subject;
        preg_match_all('/\{\{([^\}]*)\}\}/', $subject, $matches_subject);
        foreach($matches_subject[0] as $i => $match)
        {
            $subject = preg_replace( '/' . preg_quote( $match ) . '/', $context->data->fields->{$matches_subject[1][$i]}, $subject );
        }

        $mailer->setSubject($subject);

        // Pass to the appropriate view
        $body = $template_controller->render(
            array(
                'type' => 'html',
                'css' => $context->data->css,
                'template_id' => $context->data->template_id,
                'fields' => $context->data->fields
            )
        );

        $body_alt = $template_controller->render(
            array(
                'type' => 'text',
                'template_id' => $context->data->template_id,
                'fields' => $context->data->fields
            )
        );

        // If over ridden to plain text mode, set the body plain text version
        if(!$html)
        {
            $body = $body_alt;
            $body_alt = ""; // For some reason plain text only works if body alt is empty?
        }

        $mailer->setBody($body);
        $mailer->AltBody = $body_alt;

        // Send to each recipient
        $recipients = (array) KConfig::unbox($context->data->recipients);
        if($context->data->recipient) $recipients[] = $context->data->recipient;

        $statuses = array();
        $status = true;
        $errors = array();

        foreach($recipients as $recipient)
        {
            $mailer->clearAllRecipients();
            $mailer->addRecipient($recipient);
            $sent = $mailer->Send();
            $statuses[] = array(
                'email'     => $recipient,
                'status'    => $sent,
                'error'     => $mailer->ErrorInfo
            );

            if(!$sent) $status = false;

            if($context->log)
            {
                // Log the email
                $context = $this->getCommandContext();
                $context->data = array(
                    'date_sent' => date( 'Y-m-d H:i:s' ),
                    'from'      => $template->from_email,
                    'to'        => $recipient,
                    'subject'   => $subject,
                    'html'      => $body,
                    'text'      => $body_alt,
                    'sent'      => $sent,
                    'message'   => $mailer->ErrorInfo
                );
                $this->getService( 'com://site/emails.controller.email' )
                    ->add( $context );
            }

            if(!$sent && $mailer->ErrorInfo) $errors[] = $mailer->ErrorInfo;
            $mailer->ErrorInfo = null;
        }

        $context->status = $status;
        $context->sent = $statuses;

        if(!$status)
        {
            $context->setError(new KControllerException(
                'Error sending mail: '.implode(', ',$errors)
            ));
        }

        return $mailer;
    }

}
